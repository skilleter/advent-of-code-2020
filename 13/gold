#! /usr/bin/env pypy3

################################################################################
""" AOC 2020 Day 13 Gold
"""
################################################################################

import argparse
import logging

################################################################################

def parse_command_line():
    """ Parse the command line arguments """

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Enable debug output')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)

    return args

################################################################################

def read_data(args):
    """ Read the data file """

    logging.debug('-' * 80)
    logging.debug('Reading data')
    logging.debug('-' * 80)

    with open(args.infile[0], 'r') as data:
        timestamp = int(data.readline())
        busses = []
        for i, bus in enumerate(data.readline().strip().split(',')):
            busses.append(0 if bus == 'x' else int(bus))

    return timestamp, busses

################################################################################

def sequential_busses(timestamp, busses):
    """ Calculate the next time the busses will depart in sequence """

    logging.debug('-' * 80)
    logging.debug('Finding next bus sequence for %s', busses)
    logging.debug('-' * 80)

    max_bus = max(busses)
    max_bus_index = busses.index(max_bus)

    t = max_bus - max_bus_index

    while True:
        for i, bus in enumerate(busses):
            if bus and (t+i) % bus != 0:
                logging.debug('Index %d fail at bus %d', t, bus)
                break

        else:
            print('Result: %d' % t)
            break

        t += max_bus

################################################################################

def main():
    """ Main function """

    args = parse_command_line()

    timestamp, busses = read_data(args)

    sequential_busses(timestamp, busses)

################################################################################

main()
