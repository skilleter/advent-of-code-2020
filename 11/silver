#! /usr/bin/env pypy3

################################################################################
""" AOC 2020 Day 11 Silver
"""
################################################################################

import argparse
import logging
import copy

################################################################################

def parse_command_line():
    """ Parse the command line arguments """

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Enable debug output')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)

    return args

################################################################################

def read_data(args):
    """ Read the data file """

    logging.debug('-' * 80)
    logging.debug('Reading data')
    logging.debug('-' * 80)

    layout = []

    with open(args.infile[0], 'r') as data:
        for entry in data.readlines():
            layout.append([ch for ch in entry.strip()])

    return layout

################################################################################

def calculate_stable_state(args, layout):
    """ Iterate until the seating layout becomes stable """

    finished = False
    count = -1
    width = len(layout[0])
    height = len(layout)

    while not finished:
        count += 1
        occupied_seats = 0

        if args.debug:
            logging.debug('-' * 80)
            logging.debug('Iteration #%d', count)
            logging.debug('-' * 80)

            for row in layout:
                logging.debug(''.join(row))

        old_layout = copy.deepcopy(layout)

        for y, row in enumerate(old_layout):
            for x, seat in enumerate(row):

                occupied = 0

                if seat in ('L', '#'):
                    if y > 0:
                        occupied += (x > 0 and old_layout[y-1][x-1] == '#') + \
                                    (old_layout[y-1][x] == '#') + \
                                    (x < width-1 and old_layout[y-1][x+1] == '#')

                    occupied +=     (x > 0 and old_layout[y][x-1] == '#') + \
                                    (x < width-1 and old_layout[y][x+1] == '#')

                    if y < height-1:
                        occupied += (x > 0 and old_layout[y+1][x-1] == '#') + \
                                    (old_layout[y+1][x] == '#') + \
                                    (x < width-1 and old_layout[y+1][x+1] == '#')

                    if seat == 'L' and not occupied:
                        layout[y][x] = '#'
                    elif seat == '#' and occupied >= 4:
                        layout[y][x] = 'L'

                    occupied_seats += (layout[y][x] == '#')

        if layout == old_layout:
            finished = True

    print('Layout reaches stability after %d iterations' % count)
    print('With %d occupied seats' % occupied_seats)

################################################################################

def main():
    """ Main function """

    args = parse_command_line()

    layout = read_data(args)

    calculate_stable_state(args, layout)

################################################################################

main()
