#! /usr/bin/env python3

################################################################################
""" AOC 2020 Day 1 Silver

    (C) 2020 John Skilleter
"""
################################################################################

import argparse

################################################################################

class Finished(Exception):
    """ Class to make exiting from a nest loop convenient """

    pass

################################################################################

def main():
    """ Main function """

    parser = argparse.ArgumentParser(description='Advent of Code Day 1 Silver')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    values = []

    with open(args.infile[0], 'r') as data:
        for entry in data.readlines():
            values.append(int(entry.strip()))

    try:
        for i, a in enumerate(values):
            for b in values[i+1:]:
                if a + b == 2020:
                    raise Finished
    except Finished:
        print('Result=%d (%d * %d)' % (a*b, a, b))

    else:
        print('No result')

################################################################################

main()
