#! /usr/bin/env python3

################################################################################
""" AOC 2020 Day 7 gold

    (C) 2020 John Skilleter
"""
################################################################################

import argparse
import logging
import re

################################################################################

MY_BAG = 'shiny gold'

################################################################################

def read_config():
    """ Read the configuration file """

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Enable debug output')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)

    rules = {}

    logging.debug('-' * 80)
    logging.debug('Reading bag rules')
    logging.debug('-' * 80)

    with open(args.infile[0], 'r') as data:
        for entry in data.readlines():
            entry = entry.strip()

            logging.debug('Rule: %s', entry)

            assert ' contain ' in entry

            entry = re.sub(' ?bags?', '', entry)

            bag, rule = entry.split(' contain ', 1)

            if rule[-1] == '.':
                rule = rule[:-1]

            assert bag not in rules

            if rule == 'no other':
                rules[bag] = []
            else:
                rule = rule.split(', ')

                rules[bag] = {}

                for r in rule:
                    num, colour = r.split(' ', 1)

                    rules[bag][colour] = int(num)

            logging.debug('  Translates to %s => %s', bag, rules[bag])

    return rules

################################################################################

def find_bags_inside(bag, rules):
    """ Recursively calculate how many bags are required inside a bag
        of a given colour """

    total = 0
    for colour in rules[bag]:
        total += (find_bags_inside(colour, rules) + 1)* rules[bag][colour]

    return total

################################################################################

def nested_bags(rules):
    """ Calculate the number of bags required within a shiny gold bag """

    total = find_bags_inside('shiny gold', rules)

    print('Total bags = %d' % total)

################################################################################

def main():
    """ Main function """

    rules = read_config()
    nested_bags(rules)

################################################################################

main()
