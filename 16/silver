#! /usr/bin/env python3

################################################################################
""" AOC 2020 Day Silver
"""
################################################################################

import sys
import argparse
import logging
import collections
import pudb

################################################################################

def parse_command_line():
    """ Parse the command line arguments """

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Enable debug output')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)

    return args

################################################################################

CONSTRAINTS = 0
MY_TICKET = 1
NEARBY_TICKETS = 2

def read_data(args):
    """ Read the data file """

    logging.debug('-' * 80)
    logging.debug('Reading data')
    logging.debug('-' * 80)

    constraints = {}
    my_ticket = []
    nearby_tickets = []

    section = CONSTRAINTS

    with open(args.infile[0], 'r') as infile:
        for data in infile:
            data = data.strip()

            if data:
                if data == 'your ticket:':
                    section = MY_TICKET
                elif data == 'nearby tickets:':
                    section = NEARBY_TICKETS
                elif section == CONSTRAINTS:
                    field, restrictions = data.split(':', 1)

                    assert field not in constraints

                    restriction_fields = restrictions.split(' or ')
                    constraints[field] = []
                    for restriction in restriction_fields:
                        values = restriction.split('-')

                        constraints[field] += [int(x) for x in values]

                elif section == MY_TICKET:
                    my_ticket = [int(x) for x in data.split(',')]

                elif section == NEARBY_TICKETS:
                    nearby_tickets.append([int(x) for x in data.split(',')])

    logging.debug('Ticket information')
    logging.debug('   Constraints: %s', constraints)
    logging.debug('   My ticket: %s', my_ticket)
    logging.debug('   Nearby tickets: %s', nearby_tickets)

    return constraints, my_ticket, nearby_tickets

################################################################################

def validate_tickets(constraints, nearby_tickets):
    """ Validate the tickets """

    logging.debug('-' * 80)
    logging.debug('Validating tickets')
    logging.debug('-' * 80)

    scan_error = 0

    for ticket in nearby_tickets:
        for field in ticket:
            valid = False
            for con in constraints:
                if (field >= constraints[con][0] and field <= constraints[con][1]) or \
                   (field >= constraints[con][2] and field <= constraints[con][3]):
                    valid = True
                    break

            if not valid:
                logging.debug('Ticket %s failed on field %d', ticket, field)
                scan_error += field

    print('Scan error rate: %d' % scan_error)

################################################################################

def main():
    """ Main function """

    args = parse_command_line()

    constraints, _, nearby = read_data(args)

    validate_tickets(constraints, nearby)

################################################################################

main()
