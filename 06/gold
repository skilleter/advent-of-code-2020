#! /usr/bin/env python3

################################################################################
""" AOC 2020 Day 6 Gold

    (C) 2020 John Skilleter
"""
################################################################################

import argparse
import logging

################################################################################

def main():
    """ Main function """

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Enable debug output')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)

    total = 0
    party = {}
    party_size = 0

    # TODO: Due to readlines() strippng the last blank line from the input file,
    # in order to process the last entry in the file it must end with TWO blank
    # lines.

    with open(args.infile[0], 'r') as data:
        for entry in data.readlines():
            entry = entry.strip()

            if entry:
                party_size += 1
                for answer in entry:
                    party[answer] = party.get(answer, 0) + 1

            else:
                all_answer = 0

                for answer in party:
                    if party[answer] == party_size:
                        all_answer += 1

                total += all_answer

                logging.debug('Party = %s, result = %d, total = %d', party, all_answer, total)

                party = {}
                party_size = 0

    print('Result = %d' % total)

################################################################################

main()
