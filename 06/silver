#! /usr/bin/env python3

################################################################################
""" AOC 2020 Day 6 Silver

    (C) 2020 John Skilleter
"""
################################################################################

import argparse
import logging

################################################################################

def main():
    """ Main function """

    parser = argparse.ArgumentParser(description='Advent of Code')
    parser.add_argument('--debug', action='store_true', help='Enable debug output')
    parser.add_argument('infile', nargs=1, action='store')

    args = parser.parse_args()

    logging.basicConfig(level=logging.DEBUG if args.debug else logging.ERROR)

    total = 0

    # TODO: Due to readlines() strippng the last blank line from the input file,
    # in order to process the last entry in the file it must end with TWO blank
    # lines.

    with open(args.infile[0], 'r') as data:
        answers = ''
        for entry in data.readlines():
            entry = entry.strip()
            answers += entry

            if entry == '':
                logging.debug('Answers = %s', answers)

                count = 0

                for l in range(0, 26):
                    if chr(l+ord('a')) in answers:
                        count += 1

                logging.debug('Count = %d', count)

                answers = ''
                total += count

    print('Result = %d' % total)

################################################################################

main()
